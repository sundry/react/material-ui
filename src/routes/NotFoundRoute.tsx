import React from 'react';
import { Redirect, Route } from 'react-router-dom';

const NotFoundRoute: React.FC = ({ ...rest }) => (
  <Route
    {...rest}
    render={() => (
      <Redirect
        to={{
          pathname: '/',
        }}
      />
    )}
  />
);

export default NotFoundRoute;
