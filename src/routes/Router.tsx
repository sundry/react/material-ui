import React from 'react';
import { Switch, Router, Route } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import Routes from './Routes';
import NotFoundRoute from './NotFoundRoute';

const history = createBrowserHistory();

const HistoryRouter: React.FC = () => {
  return (
    <Router history={history}>
      <Switch>
        {Routes.map((props: any, key: any) => (
          <Route key={key} {...props} />
        ))}
        <NotFoundRoute />
      </Switch>
    </Router>
  );
};

export default HistoryRouter;
