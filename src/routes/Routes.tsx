import Home from 'views/Home/Home';

const Routes = [
  {
    path: '/',
    component: Home,
    exact: true,
  },
];

export default Routes;
