import React from 'react';
import {
  WithStyles,
  Theme,
  createStyles,
  withStyles,
} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const styles = (theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      height: '100%',
    },
  });

interface Props extends WithStyles<typeof styles> {}

const Home: React.FC<Props> = ({ classes }: Props) => {
  return (
    <Grid
      className={classes.root}
      container
      justify="center"
      alignItems="center"
    >
      <Grid item>Home</Grid>
    </Grid>
  );
};

export default withStyles(styles)(Home);
