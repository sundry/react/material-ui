import React from 'react';
import Router from 'routes/Router';
import { MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Theme from 'themes/Theme';

const App: React.FC = () => {
  return (
    <MuiThemeProvider theme={Theme}>
      <CssBaseline />
      <Router />
    </MuiThemeProvider>
  );
};

export default App;
